## Nama Anggota:
### -Mohammad Firman Fardiansyah 5025201109
### -Gery Febrian Setyara 5025201151
### -Fayyadh Hafizh 5025201164

## Soal 1
Pada soal 1, kita diminta untuk membuat program yang dapat meng gacha diwaktu tertentu dan dengan syarat tertentu

### Poin A: Mendownload dan Mengekstrak File dengan Program
Untuk menyelesaikan ini kita harus mendownload dulu filenya dari link google drive yang telah diberikan
    ` 

    else if(pid1 == 0 && pid2 > 0)
    { 
    char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "db_weapons.zip", NULL};
    if(execv(args[0],args+1) == -1) 
    {
    perror("wget weapons failed!");
    exit(EXIT_FAILURE);
    }
    }
    else if(pid1 > 0 && pid2 == 0)
    {
    char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "db_characters.zip", NULL};
    if(execv(args[0],args+1) == -1) 
    {
    perror("wget characters failed!");
    exit(EXIT_FAILURE);
    }
    }

Program akan mengecek pid,jika sesuai maka program akan mendownload Weapon dan Character dari link docs.google yang telah diberikan


    init_databases();
    checkpid(daemon_pid);
    
    makedir("gacha_gacha");
    
lalu file akan membuat directory gacha_gacha untuk sebagai tempat menyimpan file yang telah digacha


### Poin B: Gacha Genap dan Ganjil
program diminta apabila gacha merupakan ganjil maka program akan meng gacha item Character dan apabila genap maka akan meng gacha weapon 
    if(jumlah_gacha%2==0){
    strcpy(type,"weapon");
    numberoffiles = dirsizes[0];
    }else{
    strcpy(type,"character");
    numberoffiles = dirsizes[1];
    }


lalu diminta untuk setiap mod 10 maka program akan membuat file txt baru dan jika jumlah gacha adalah mod 90 maka program akan membuat folder baru
    
    do
    {
    format_name(filename,foldername,jumlah_gacha);

    if((jumlah_gacha%90)==0)
    makedir(foldername);

    fp = fopen(filename,"w");
    do
    {
    curr_primogems -= GACHA_COST;
    if(curr_primogems > 0)
    break;
    format_gachatxt(gachabuffer,dirsizes,jumlah_gacha,curr_primogems);
    jumlah_gacha++;
    }
    while(jumlah_gacha%10!=0);
    sleep(1);
    fclose(fp);

### Poin C: Format Penamaan Gacha
program diminta untuk merename file yang telah di gacha sesuai dengan jam berapakah file tersebut di gacha dan berapa total gacha yang dilakukan oleh user

    sprintf(foldername, "gacha_gacha/total_gacha_%d",(jumlah_gacha/90)*90);
    sprintf(filename, "%s/%02d:%02d:%02d_gacha_%d.txt",foldername ,timeinfo->tm_hour, timeinfo->tm_min,     timeinfo->tm_sec,(jumlah_gacha/10)*10);
    
karena permintaan yang perlu memasukkan waktu maka diawal program harus di set time dari program tersebut

### Poin D: Sistem Gacha
untuk menyelesaikan ini kita perlu menginilisiasi dulu gacha costnya sebanyak 160 sesuai di gamenya,lalu di primogems nya juga diatur seangka 79000

    #define GACHA_COST 160
    int curr_primogems = 79000;
    int jumlah_gacha = 0;
    get_json_value(fileloc,name,rarity);
    sprintf(output,"%d_%s_%s_%s_%d\n",jumlah_gacha,type,rarity,name,sisa_primogems);
    }

dengan sprintf.... maka program akan dapat mengoutputkan hasil gacha dalam bentuk txt dengan format jumlah_gacha,type,rarity,name,sisa_primogems

### Poin E: Waktu Gacha
disini diminta untuk melakukan gacha dilakukan pada waktu tanggal 30 maret jam 04:44 sebagai wujud anniversary maka di inilisiasi dulu waktu startnya 

    start = get_epoch("2022-03-30 04:44:00");
    end = get_epoch("2022-03-30 07:44:00");
    
dan diminta setelah 3 jam yaitu jam 07:44 maka semua folder akan di delete dan membuat folder baru dengan nama not_safe_for_wibu dengan password satuduatiga
     
    else if(pid == 0)
    { 
    char *args[] = {"/bin/zip", "zip", "-rq", "--password", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha/", NULL};
    if(execv(args[0],args+1) == -1) 
    {
    perror("zip failed!");
    exit(EXIT_FAILURE);
    }
    }
    else
    {
    perror("child failed!");
    exit(EXIT_FAILURE);
    }












## Soal 2
Pada soal 2, kita diberikan sebuah file berbentuk zip yang nantinya harus kita unzip dan file-file tertentu dalam folder tersebut harus dipindahkan sesuai dengan perintah dari soal.

### Poin A: Unzip file, masukkan dalam suatu folder, dan hapus file tidak penting
Untuk menyelesaikan poin ini, maka kita bisa melakukan membuat folder terlebih dahulu sesuai dari perintah dari soal
    
    `
    if(child_id == 0) {
	    char *argv[] = {"mkdir","-p","/home/firman/shift2/drakor",NULL}; //buat folder
        execv("/bin/mkdir", argv);
    }	
    while((wait(&status))>0); //parent
    unzip();
    `

Setelah itu kita masuk ke fungsi unzip untuk melakukan unzip folder drakor. Disini kita menggunakan -x agar tidak melakukan unzip terhadap file yg spesifik, karena file penting (.png) ada di luar folder semua.
    
    `
    void unzip() { //unzip file yang telah didownload
    	pid_t child_id;
    	child_id = fork();

    	if(child_id < 0) {
       		exit(EXIT_FAILURE);
    	}

    	if (child_id == 0) {
        	char *argv[] = {"unzip","-q","/home/firman/Downloads/drakor.zip","-x","*/*","-d","/home/firman/shift2/drakor",NULL}; 
        	execv("/bin/unzip", argv);
	    } 
	while (wait(&status)>0);
    };
    `

### Poin B: Membuat folder sesuai genre drakor secara otomatis
Untuk menjawab poin b, kita terlebih dahulu memisahkan string per poster drakor menggunakan strtok_r. Sehingga nanti akan mendapatkan string untuk genre drakor itu sendiri, untuk penjelasan ini akan didetail di Poin D. Kemudian setelah didapatkan, string tersebut disimpan dalam genre yang kemudian digabungkan dengan source yang berisi folder utama menggunakan strcat.
    
    `
    char text[100], tmpsrc1[100], tmpsrc2[100], source[100]="/home/firman/shift2/drakor/";

    strcat(source, genre); //gabungkan untuk membuat nama folder berdasarkan genre
        char *argv[] = {"mkdir","-p",source,NULL};//buat folder jika sebelumnya tidak ada
        exefk("/bin/mkdir",argv);
    `

Karena pasti ada genre yang sama, maka digunakan -p untuk membuat folder yang sebelumnya tidak ada saja. Kemudian disini exefk merupakan fungsi tersendiri agar mempercepat fork karena tidak perlu menunggu childnya selesai.
    
    `
    void exefk(char command[], char *argv[]) { //agar dapat exe tanpa menunggu child selesai
        pid_t child_id;
        child_id = fork();

        if(child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if(child_id == 0) {
                execv(command, argv);
        }
        while ((wait(&status)) > 0);
    }
    `
    
### Poin C: Memindahkan poster ke folder baru dan rename dengan nama baru
Untuk menyelesaikan poin ini, kita memisahkan string dengan strtok_r yang akan dijelaskan detail pada Poin D. Ketika sudah mendapatkan string untuk nama posternya, maka kita gabungkan terlebih dahulu dengan .png karena sebelumnya telah diremove (akan dijelaskan di Poin D). Setelah sesuai format, kita dapatkan file posternya posternya setelah kita pindahkan ke folder baru dan kita rename ulang.
    
    `
    char text[100], tmpsrc1[100], tmpsrc2[100], source[100]="/home/firman/shift2/drakor/";
    strcpy(text, readDir -> d_name); //nama poster yang lengkap
    strcpy(tmpsrc1, source);

    strcat(namaDrakor,".png"); //namaDrakor berisi nama posternya setelah dipisahkan stringnya 
    strcat(tmpsrc1, text);

    strcpy(tmpsrc2, source);
    strcat(tmpsrc2,"/");
    strcat(tmpsrc2, text);
    strcat(source,"/");
    strcat(source, namaDrakor);

    char *chgNama[] = {"mv",tmpsrc2,source,NULL}; //ubah namanya sesuai format
        exefk("/bin/mv",chgNama);
    `

### Poin D: Pisahkan dan copy poster ketika dalam 1 foto terdapat 2 poster
Pertama-tama kita mendeklarasikan struct dirent untuk membaca list dalam foldernya. Kemudian kita dapatkan nama file posternya dengan d_name, tetapi nama file tersebut masih ada .png nya. Sehingga akan kita hapus terlebih dahulu untuk memudahkan pemisahan string menggunakan strtok_r.
    
    `
    char src[101] = "/home/firman/shift2/drakor";
    drakor = opendir(src); //open directorinya
	struct dirent *readDir; //membaca list folder dengan dirent
    
	if(drakor != NULL) {
        while((readDir = readdir(drakor))!= NULL){
            if(readDir -> d_type == DT_REG){ //jika file bertipe reguler
                char *namaPoster = readDir -> d_name;
                char *tanpaPNG = rmv(namaPoster);
                char *tmp1, *tmp2, *tmp3, *tmp4;
    `

Kemudian kita akan masuk ke fungsi rmv untuk menghilangkan .png di akhir nama file
    
    `
    char* rmv(char* name){ //untuk menghilangkan format .png di nama
        int i, j, tmp;
        for(i = 0; name[i] != '\0'; i++);{
            tmp = i - 3;
        }
        if(tmp < 1){
            return NULL;
        }
        char* del = (char*) malloc (tmp * sizeof(char));

        for(j = 0; j < tmp - 1; j++){
	        del[j] = name[j];
        }
        del[j] = '\0';
        return del;
    }
    `

Setelah kita dapatkan nama baru tanpa png, maka kita akan memisahkan string dengan delimiter (_) karena format tersebut adalah 2 buah poster dalam 1 file/foto.
    
    `
    for(tmp1 = strtok_r(tanpaPNG,"_",&tmp3); tmp1 != NULL; tmp1 = strtok_r(NULL,"_",&tmp3)) {
    `

Setelah kita pisahkan string dengan delimiter (_), selanjutnya kita pisahkan dengan delimiter (;) untuk mendapatkan nama, tahun rilis, dan genre poster.
    
    `
    int strnum = 0;
	char namaDrakor[100], tahun[100], genre[100];
                        
    for(tmp2 = strtok_r(tmp1,";",&tmp4); tmp2 != NULL; tmp2 = strtok_r(NULL,";",&tmp4)) { 
        if(strnum == 0) 
            strcpy(namaDrakor, tmp2);//simpan nama
        if(strnum == 1) 
            strcpy(tahun, tmp2);//simpan tahun
        if(strnum == 2) 
  			strcpy(genre, tmp2);//simpan genre
        strnum++;
    }
    `

Selanjutnya, kita pindahkan dan copy ulang file dengan 2 poster dalam 1 file menggunakan command cp.
    
    `
    char text[100], tmpsrc1[100], tmpsrc2[100], source[100]="/home/firman/shift2/drakor/";
    strcpy(text, readDir -> d_name);
    strcpy(tmpsrc1, source);

    strcat(tmpsrc1, text);
    
    char *cpy[] = {"cp","-r", tmpsrc1, source, NULL}; //copy filenya
        exefk("/bin/cp",cpy);
    `

### Poin E: Membuat file data.txt yang berisi genre, nama, dan tahun rilis poster
Pertama-tama kita akan membuat file data.txt terlebih dahulu, kemudian kita isikan kategori/genre, nama poster, dan tahun rilis. Pengisian tersebut dengan cara menggabungkan berbagai string dengan strcat yang akhirnya disimpan pada tmp. Setelah itu kita open filenya dan string dalam tmp akan dipindah ke dalam file tersebut
    
    `
    strcat(datatxt,"/data.txt");
			   
	char tmp[100];
    strcpy(tmp,"kategori : ");
    strcat(tmp, genre);
    strcat(tmp,"\n\nnama : ");
    strcat(tmp, txtNama);
    strcat(tmp,"\nrilis: tahun ");
    strcat(tahun,"\n\n");
    strcat(tmp, tahun);

    FILE *fdata;
    fdata = fopen(datatxt,"a");
    fputs(tmp, fdata);
    fclose(fdata);
    `
    
### Kendala
Masih belum bisa melakukan sorting tahun rilis genre, kemudian belum bisa memunculkan kategori/genre sekali saja.

## Soal 3
ada soal kali, pertama-tama kita akan membuat dua direktori, yaitu direktori 'darat' dan 'air'. Pembuatan direktori 'air' dilakukan setelah direktori 'darat' dibuat dengan selang 3 detik. Setelah itu kita akan mengekstrak file 'animal.zip' pada folder yang telah ditentukan. Setelah itu, kita memisahkan tiap file hasil ektraksi pada folder 'darat' dan 'air' tergantung dari nama filenya (jika mengandung kata 'darat' maka akan dipindahkan ke direktori darat begitu pula sebaliknya). Untuk file yang namanya tidak memiliki kata 'darat' maupun 'air' akan dihapus. Pada direktori 'darat', setiap nama file yang memiliki kata 'bird' akan dihapus. Pada direktori 'air' kita akan membuat sebuah file dengan nama 'list.txt' yang nantinya akan kita isi dengan list nama hewan yang ada pada direktori 'air' .

### a.
Pertama, kita akan membuat dua direktori dengan nama 'darat' dan 'air'. Pembuatan direktori 'air' dilakukan setelah pembuatan direktori 'darat' dengan delay selama tiga detik.

![1](./soal3/screenshot/1.jpg)

### b.
Kedua, kita akan mengekstrak file 'animal.zip' pada direktori yang telah ditentukan pada soal.

![2](./soal3/screenshot/2.jpg)

### c.
Ketiga, kita akan memisahkan tiap file sesuai kata 'air' dan 'darat' yang terdapat pada namanya ke masing-masing direktori. Untuk file yang tidak mengandung kata 'darat' maupun 'air' pada namanya akan dihapus.

![3](./soal3/screenshot/3.jpg)
![4](./soal3/screenshot/4.jpg)

### d.
Keempat, pada direktori 'darat' kita akan menghapus semua file yang mengandung kata 'bird' pada namanya.

![5](./soal3/screenshot/5.jpg)

### e.
Terakhir, kita akan membuat file dengan namanya 'list.txt' dan kita akan menulis list nama hewan sesuai format pada soal.

![6](./soal3/screenshot/6.jpg)
![7](./soal3/screenshot/7.jpg)

### Kendala
Selama masa pengerjaan soal ini, saya masih belum terlalu paham bagaimana cara kerja fork dan exec selain untuk mempercepat kerja dengan dua 'pipe' yang berbeda. Selain itu saya juga kesusahan pada bagian mengambil nama file dengan wildcard karena saya sejujurnya masih bingung dengan beberapa wildcard yang ada.
