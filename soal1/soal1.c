#define _XOPEN_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <wait.h>
#include <limits.h>
#include <time.h>
#include <dirent.h>
#include <json-c/json.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

#define GACHA_COST 160

// prints out a readable format from time_t
void check_epoch(time_t rawtime)
{
    struct tm * timeinfo;
    timeinfo = localtime(&rawtime);
    char buf[255];

    strftime(buf,sizeof(buf),"%Y-%m-%d %H:%M:%S",timeinfo);
    puts(buf);
}

// convert
time_t get_epoch(char *timestamp)
{
    struct tm timeinfo;
    memset(&timeinfo, 0, sizeof(timeinfo));
    strptime(timestamp, "%Y-%m-%d %H:%M:%S", &timeinfo);
    timeinfo.tm_isdst = -1;
    return mktime(&timeinfo);
}

// random number generator from 0 to max
int random_max(int max)
{
    int range = max;
    return rand() % range;
}

// JSON parser, gets the name and rarity values
void get_json_value(char *filename, char *name_val, char *rarity_val)
{
    FILE *fp;
    char buffer[4095];


    struct json_object *parsed_json;
    struct json_object *rarity;
    struct json_object *name;

    fp = fopen(filename,"r");
    fread(buffer, 4095, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json,"rarity",&rarity);
    json_object_object_get_ex(parsed_json,"name",&name);

    sprintf(name_val,"%s",json_object_get_string(name));
    sprintf(rarity_val,"%s",json_object_get_string(rarity));
}

// prevent any process other than the specified pid form passing this point
void checkpid(pid_t pid)
{
    if(getpid() != pid)
    {
        exit(EXIT_SUCCESS);
    }
}

// get gacha text content
void format_gachatxt(char *output, int dirsizes[] ,int jumlah_gacha, int sisa_primogems){
    char name[128];
    char rarity[8];
    char type[12];
    char fileloc[PATH_MAX];
    int numberoffiles;
    
    if(jumlah_gacha%2==0){
        strcpy(type,"weapon");
        numberoffiles = dirsizes[0];
    }else{
        strcpy(type,"character");
        numberoffiles = dirsizes[1];
    }

    sprintf(fileloc,"./%ss/%d.json",type,random_max(numberoffiles));
    // puts(fileloc); //debug only
    get_json_value(fileloc,name,rarity);
    sprintf(output,"%d_%s_%s_%s_%d\n",jumlah_gacha,type,rarity,name,sisa_primogems);
}

// get the filename format for the txt file which include a timestamp
void format_name(char *filename, char *foldername, int jumlah_gacha)
{
    time_t rawtime;
    struct tm * timeinfo;
    
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    
    sprintf(foldername, "gacha_gacha/total_gacha_%d",(jumlah_gacha/90)*90);
    sprintf(filename, "%s/%02d:%02d:%02d_gacha_%d.txt",foldername ,timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec,(jumlah_gacha/10)*10);
}

// rename() substitute
void rename_file(char *oldname, char *newname)
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        // printf("was waiting for (%d) in rename on ppid of (%d)\n",waiter,getppid()); //debug only
    }
    else if(pid == 0)
    { 
        // printf("oldname: %s\nnewname: %s\n",oldname,newname); //debug only
        char *args[] = {"/bin/mv", "mv", oldname , newname, NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("rename failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("rename failed!");
        exit(EXIT_FAILURE);
    }
}

// count all files that are in the directory
int count_files(char *dirpath)
{
    struct dirent *de;  
    int counter = 0;

    DIR *dr1 = opendir(dirpath);
  
    if (dr1 == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory" );
        exit(EXIT_FAILURE);
    }
  
    while ((de = readdir(dr1)) != NULL) counter++;
    closedir(dr1); 

    return counter - 2;

}

// rename all files in the directory in an sequential number
void rename_numerical()
{
    struct dirent *de;  
    char fileloc[PATH_MAX];
    char newname[PATH_MAX];
    int counter = 0;

    DIR *dr1 = opendir("./weapons");
  
    if (dr1 == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory" );
        exit(EXIT_FAILURE);
    }
  
    while ((de = readdir(dr1)) != NULL)
    {
        sprintf(fileloc,"./weapons/%s", de->d_name);
        if(strcmp(fileloc,"./weapons/.")!=0&&strcmp(fileloc,"./weapons/..")!=0)
        {
            sprintf(newname,"./weapons/%d.json", counter++);
            rename_file(fileloc,newname);
        }
        //sleep(0.10);
    }
  
    closedir(dr1);    

    counter = 0;

    DIR *dr2 = opendir("./characters");
  
    if (dr2 == NULL)  // opendir returns NULL if couldn't open directory
    {
        printf("Could not open current directory" );
        exit(EXIT_FAILURE);
    }
  
    while ((de = readdir(dr2)) != NULL)
    {
        sprintf(fileloc,"./characters/%s", de->d_name);
        if(strcmp(fileloc,"./characters/.")!=0&&strcmp(fileloc,"./characters/..")!=0)
        {
            sprintf(newname,"./characters/%d.json", counter++);
            rename_file(fileloc,newname);
        }
    }
  
    closedir(dr2);  
}

// mkdir() substitute
void makedir(char *namedir)
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        // printf("was waiting for (%d) in mkdir on ppid of (%d)\n",waiter,getppid()); //debug only
    }
    else if(pid == 0)
    { 
        char *args[5] = {"/bin/mkdir", "mkdir", "-p" , namedir, NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("mkdir failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}

// removes downloaded zip database
void rm_init_databases()
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        printf("was waiting for (%d) in rm_init on ppid of (%d)\n",waiter,getppid()); //debug only
        rename_numerical();
    }
    else if(pid == 0)
    { 
        char *args[] = {"/bin/rm", "rm", "-f", "db_weapons.zip", "db_characters.zip",NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("rm failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}

// unzips downloaded zip database then forwards to rm_init_databases()
void unzip_databases()
{
    pid_t pid1,pid2,waiter;
    pid1 = fork();
    pid2 = fork();

    
     if(pid1 > 0 && pid2 > 0) 
    {
        // waiter = printf("unzip %d %d\n",pid1,pid2); //debug only
        wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        waiter =wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        //start
        rm_init_databases();
        //end
    }
    else if(pid1 == 0 && pid2 > 0)
    { 
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "db_weapons.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("unzip weapons failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/unzip", "unzip", "-qq", "-n", "db_characters.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("unzip characters failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 < 0 && pid2 < 0)
    {
        perror("unzip childs failed!");
        exit(EXIT_FAILURE);
    }
}

// downloads the data into a zip file then forwards to unzip_databases()
void init_databases()
{
    pid_t pid1, pid2, waiter;
    pid1 = fork();
    pid2 = fork();

    if(pid1 > 0 && pid2 > 0) 
    {
        // printf("wget %d %d\n",pid1,pid2); //debug only
        waiter = wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        waiter = wait(NULL);
        // printf("was waiting for (%d)\n",waiter); //debug only
        //start
        unzip_databases();
        //end
    }
    else if(pid1 == 0 && pid2 > 0)
    { 
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "db_weapons.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("wget weapons failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 > 0 && pid2 == 0)
    {
        char *args[] = {"/bin/wget", "wget", "--quiet" , "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "db_characters.zip", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("wget characters failed!");
                exit(EXIT_FAILURE);
        }
    }
    else if(pid1 < 0 && pid2 < 0)
    {
        perror("wget childs failed!");
        exit(EXIT_FAILURE);
    }
}

// removes all working folders.
void rm_all()
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        printf("was waiting for (%d) in rm_all on ppid of (%d)\n",waiter,getppid()); //debug only
    }
    else if(pid == 0)
    { 
        char *args[] = {"/bin/rm", "rm", "-rf", "./weapons", "./characters", "./gacha_gacha",NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("rmd failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}

// zips the gacha_gacha directory with password, then forwards to rm_all()
void zip_gacha()
{
    pid_t pid,waiter;
    pid = fork();

    if(pid > 0) 
    {
        waiter = wait(NULL);
        printf("was waiting for (%d) in zip on ppid of (%d)\n",waiter,getppid()); //debug only
        rm_all();

    }
    else if(pid == 0)
    { 
        char *args[] = {"/bin/zip", "zip", "-rq", "--password", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha/", NULL};
        if(execv(args[0],args+1) == -1) 
        {
                perror("zip failed!");
                exit(EXIT_FAILURE);
        }
    }
    else
    {
        perror("child failed!");
        exit(EXIT_FAILURE);
    }
}

// runs the gacha program
void run_gacha(){
    int curr_primogems = 79000;
    int jumlah_gacha = 0;
    char foldername[PATH_MAX];
    char filename[PATH_MAX];
    char gachabuffer[128];
    int dirsizes[2];
    dirsizes[0] = count_files("./weapons");
    dirsizes[1] = count_files("./characters");
    FILE *fp;


    do
    {
        format_name(filename,foldername,jumlah_gacha);

        if((jumlah_gacha%90)==0)
            makedir(foldername);

        fp = fopen(filename,"w");
        do
        {
            curr_primogems -= GACHA_COST;
            if(curr_primogems > 0)
                break;
            format_gachatxt(gachabuffer,dirsizes,jumlah_gacha,curr_primogems);
            //puts(gachabuffer); //debug only
            fputs(gachabuffer, fp);
            jumlah_gacha++;
        }
        while(jumlah_gacha%10!=0);
        sleep(1);
        fclose(fp);

        //if(jumlah_gacha >= 150)break; //debug only
        
    }
    while(curr_primogems > 0);
}

int main ()
{

    time_t start,end;
    start = get_epoch("2022-03-30 04:44:00");
    end = get_epoch("2022-03-30 07:44:00");
    pid_t daemon_pid,sid;

    // puts("start time:");
    // check_epoch(start);
    // puts("start time:");
    // check_epoch(end);

    // converting into daemon
    daemon_pid = fork();
    if (daemon_pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (daemon_pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);



    // get actual pid
    daemon_pid = getpid();

    checkpid(daemon_pid);

    // point a
    init_databases();
    checkpid(daemon_pid);
    
    makedir("gacha_gacha");
    
    // point b, c, & d
    checkpid(daemon_pid);
    while(time(NULL) < start); //waits until Wed Mar 30 2022 04:44:00 GMT+0700
    run_gacha();
    
    // point e
    checkpid(daemon_pid);
    while(time(NULL) < end); //waits until Wed Mar 30 2022 07:44:00 GMT+0700
    zip_gacha();

    // terminate remaining childs
    checkpid(daemon_pid);
    return 0;
}